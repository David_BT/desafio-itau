//
//  NetworkManagerTests.swift
//  Desafio-ItauTests
//
//  Created by David Batista on 17/07/21.
//

import XCTest

class NetworkManagerTests: XCTestCase {

    var networkManager = NetworkManager<ItauAPI, [ReleaseModel]>()

    func testGetReleases() throws {
        let expectation = XCTestExpectation(description: "API Itau")

        networkManager.request(target: .getReleases) { result in
            switch result {
            case .success(let data):
                XCTAssert(data is [ReleaseModel])
            case .failure:
                XCTFail("API Error")
            }

            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 10.0)
    }
}
