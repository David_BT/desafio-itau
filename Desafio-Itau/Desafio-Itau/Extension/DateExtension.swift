//
//  DateExtension.swift
//  Desafio-Itau
//
//  Created by David Batista on 17/07/21.
//

import Foundation

extension Date {

    static func getHumanReadable(month: Int) -> String {
        let weekdays = [
            "Janeiro",
            "Fevereiro",
            "Março",
            "Abril",
            "Maio",
            "Junho",
            "Julho",
            "Agosto",
            "Setembro",
            "Outubro",
            "Novembro",
            "Dezembro"
        ]

        return weekdays[month - 1]
    }
}
