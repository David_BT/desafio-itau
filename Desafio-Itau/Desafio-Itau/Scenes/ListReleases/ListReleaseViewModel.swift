//
//  ListReleaseViewModel.swift
//  Desafio-Itau
//
//  Created by David Batista on 17/07/21.
//

import Foundation

protocol ListReleaseViewModelProtocol {
    var releasesUseCase: ReleaseListUseCaseProtocol { get }
    var releaseList: [ReleaseModel] { get }

    func getReleases(onSucess: @escaping (() -> Void),
                     onFailure: @escaping ((NetworkError?) -> Void))
}

class ListReleaseViewModel: ListReleaseViewModelProtocol {
    internal var releasesUseCase: ReleaseListUseCaseProtocol
    var releaseList: [ReleaseModel] = []

    init(releasesUseCase: ReleaseListUseCaseProtocol) {
        self.releasesUseCase = releasesUseCase
    }

    func getReleases(onSucess: @escaping (() -> Void), onFailure: @escaping ((NetworkError?) -> Void)) {

        releasesUseCase.get { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .success(let releases):
                self.releaseList = releases.sorted(by: { releaseOne, releaseTwo in
                    releaseOne.entryMoth < releaseTwo.entryMoth
                })
                onSucess()

            case .failure(let error):
                onFailure(error)
            }
        }
    }
}
