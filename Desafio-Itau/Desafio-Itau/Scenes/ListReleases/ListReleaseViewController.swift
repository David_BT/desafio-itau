//
//  ListReleaseViewController.swift
//  Desafio-Itau
//
//  Created by David Batista on 17/07/21.
//

import UIKit

class ListReleaseViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!

    // MARK: Varibles
    private let viewModel: ListReleaseViewModelProtocol

    // MARK: Init
    init(viewModel: ListReleaseViewModelProtocol) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("Use init(viewModel:coder:)")
    }

    // MARK: App LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpTableView()
        loadReleases()
    }

    func setUpTableView() {
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 250
        tableView.rowHeight = UITableView.automaticDimension

        tableView.register(UINib(nibName: ReleaseTableViewCell.reuseIdentifier, bundle: nil),
                           forCellReuseIdentifier: ReleaseTableViewCell.reuseIdentifier)
    }

    func loadReleases() {

        viewModel.getReleases {
            self.tableView.reloadData()
        } onFailure: { error in
            print("Falha")
        }
    }

}

extension ListReleaseViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.releaseList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReleaseTableViewCell.reuseIdentifier)
                as? ReleaseTableViewCell else { fatalError() }
        cell.setCell(with: viewModel.releaseList[indexPath.row])

        return cell
    }
}
