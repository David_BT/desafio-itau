//
//  ReleaseTableViewCell.swift
//  Desafio-Itau
//
//  Created by David Batista on 17/07/21.
//

import UIKit

class ReleaseTableViewCell: UITableViewCell {

    static let reuseIdentifier = "ReleaseTableViewCell"

    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    // MARK: Variables
    let formatter = NumberFormatter()
    let nameFormatter = DateFormatter()

    func setCell(with release: ReleaseModel) {

        monthLabel.text = Date.getHumanReadable(month: release.entryMoth)
        valueLabel.text = formatterCurrency(release.value)
        descriptionLabel.text = release.origem
    }

    internal func formatterCurrency(_ value: Float) -> String {
        formatter.locale = Locale.current
        formatter.numberStyle = .currency

        return formatter.string(from: value as NSNumber) ?? ""
    }
}


/*
 {
     "id": 1,
     "valor": 13.3,
     "origem": "Uber",
     "categoria": 1,
     "mes_lancamento": 1
   }
*/
