//
//  ItauAPI.swift
//  Desafio-Itau
//
//  Created by David Batista on 17/07/21.
//

import Foundation
import Moya

public enum ItauAPI {
    case getReleases
}

extension ItauAPI: TargetType {
    public var baseURL: URL {
        guard let url = URL(string: "https://desafio-it-server.herokuapp.com") else {
            fatalError("Default URL is not a valid URL")
        }

        return url
    }

    public var path: String {
        return "/lancamentos"
    }

    public var method: Moya.Method {
        return .get
    }

    public var sampleData: Data {
        return Data()
    }

    public var task: Task {
        return .requestParameters(parameters: [:], encoding: URLEncoding.queryString)
    }

    public var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }

    public var validationType: ValidationType {
        return .successCodes
    }
}
