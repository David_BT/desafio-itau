//
//  NetworkManager.swift
//  Desafio-Itau
//
//  Created by David Batista on 17/07/21.
//

import Foundation
import Moya

class NetworkManager<T: TargetType, D: Decodable> {
    private var provider = MoyaProvider<T>(plugins: [NetworkLoggerPlugin()])

    func request(target: T, completion: @escaping (Result<D, NetworkError>) -> Void) {
        provider.request(target) { result in
            switch result {
            case let .success(response):
                do {
                    let results = try JSONDecoder().decode(D.self, from: response.data)
                    completion(.success(results))
                } catch {
                    completion(.failure(.serialized))
                }
            case let .failure(error):
                switch error.response?.statusCode {
                case 400, 404:
                    completion(.failure(.notFound))
                default:
                    completion(.failure(.undefined))
                }
            }
        }
    }
}
