//
//  EntryModel.swift
//  Desafio-Itau
//
//  Created by David Batista on 17/07/21.
//

import Foundation

/*
 {
     "id": 1,
     "valor": 13.3,
     "origem": "Uber",
     "categoria": 1,
     "mes_lancamento": 1
   }
*/

struct ReleaseModel: Codable {
    let id: Int
    let value: Float
    let origem: String
    let category: Int
    let entryMoth: Int

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case value = "valor"
        case origem = "origem"
        case category = "categoria"
        case entryMoth = "mes_lancamento"
    }
}
