//
//  NetworkError.swift
//  Desafio-Itau
//
//  Created by David Batista on 17/07/21.
//

import Foundation

enum NetworkError: Error {
    case undefined
    case unauthorized
    case notFound
    case serialized
}
