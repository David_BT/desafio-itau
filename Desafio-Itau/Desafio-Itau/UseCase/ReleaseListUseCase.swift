//
//  GetReleaseList.swift
//  Desafio-Itau
//
//  Created by David Batista on 17/07/21.
//

import Foundation
import Moya

protocol ReleaseListUseCaseProtocol {
    typealias GetCompletion = (Result<[ReleaseModel], NetworkError>) -> Void

    var networkManager: NetworkManager<ItauAPI, [ReleaseModel]> { get }

    func get(completion: @escaping GetCompletion)
}

class ReleaseListUseCase: ReleaseListUseCaseProtocol {
    internal let networkManager: NetworkManager<ItauAPI, [ReleaseModel]>

    init(networkManager: NetworkManager<ItauAPI, [ReleaseModel]>) {
        self.networkManager = NetworkManager()
    }
    
    func get(completion: @escaping GetCompletion) {
        networkManager.request(target: .getReleases) { result in
            switch result {
            case .success(let releases):
                completion(.success(releases))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
