# Desafio Timeline iOS
Primeiramente obrigado por estar aqui! E querer trabalhar no Itaú Unibanco!

Neste desafio queremos entender um pouco mais do seu conhecimento em desenvolvimento iOS, método de avaliação é simples, tendo seu código em mãos iremos validar a solução que você desenvolveu para o desafio

# Requisitos

 - Swift 5 ou superior 
 - iOS 10.3 ou superior 
 - Preferencialmente não usar Frameworks externos

# Desafio

Seguir link: https://gitlab.com/desafioiOS/desafio